package com.devcamp.task6070jparelationshipordercustomer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task6070JpaRelationshipOrderCustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task6070JpaRelationshipOrderCustomerApplication.class, args);
	}

}
